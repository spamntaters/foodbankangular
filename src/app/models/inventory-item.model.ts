export class InventoryItem {
    id: Number;
    foodItemName: String;
    foodItemQuantity: Number;
    
}